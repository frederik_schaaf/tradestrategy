﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using TradeStrategy.DataDefinition;

namespace Visualizer
{

    public class PriceIndexDrawObject
    {

        public PriceIndex PriceIndex { get; set; }

        public string Name => PriceIndex.Name;

        public System.Windows.Media.Brush Stroke { get; set; } = System.Windows.Media.Brushes.Gray;

        public enum Styles
        {
            Default,
            Candle,
        }

        public Styles Style { get; set; } = Styles.Default;
        public int NumberOfDataPoints { get; set; } = 20;
        public int NumberOfXLabels { get; set; } = 10;
        public bool DrawXLabels { get; set; } = false;
        public bool DrawYLabels { get; set; } = true;
    }
}
