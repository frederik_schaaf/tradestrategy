﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Wpf;
using TradeStrategy.DataDefinition;

namespace Visualizer
{
    public partial class PriceIndexControl : UserControl
    {
        
        public PriceIndexControl()
        {
            InitializeComponent();
        }

        public string Title { get; set; } = "Price";

        // Dependency Property
        public static readonly DependencyProperty PriceIndexDrawObjectsProperty =
            DependencyProperty.Register("PriceIndexDrawObjects", typeof(ObservableCollection<PriceIndexDrawObject>),
                typeof(PriceIndexControl), new FrameworkPropertyMetadata(new ObservableCollection<PriceIndexDrawObject>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnPriceIndexDrawObjectsChanged, OnPriceIndexDrawObjectsCoerceValueCallback));

        private static void OnPriceIndexDrawObjectsChanged(DependencyObject source,
            DependencyPropertyChangedEventArgs e)
        {
            PriceIndexControl control = source as PriceIndexControl;
            control.PriceIndexDrawObjects = (ObservableCollection<PriceIndexDrawObject>)e.NewValue;
            control.PriceIndexDrawObjects.CollectionChanged += control.OnPriceIndexDrawObjectsCollectionChanged;
            control.Draw();
        }

        private void OnPriceIndexDrawObjectsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Draw();
        }

        private static object OnPriceIndexDrawObjectsCoerceValueCallback(DependencyObject source,
            object e)
        {
            return e;
        }

        // .NET Property wrapper
        public ObservableCollection<PriceIndexDrawObject> PriceIndexDrawObjects
        {
            get
            {
                return (ObservableCollection<PriceIndexDrawObject>)GetValue(PriceIndexDrawObjectsProperty);
            }
            set
            {
                SetValue(PriceIndexDrawObjectsProperty, value);
            }
        }

        class Drawer
        {
            public PriceIndexDrawObject.Styles Style { get; private set; }


            public Drawer(PriceIndexDrawObject.Styles style, int numberOfDataPoints, int numberOfXLabels, bool drawXLabels, bool drawYLabels)
            {
                NumberOfDataPoints = numberOfDataPoints;
                NumberOfXLabels = numberOfXLabels;
                Style = style;
                DrawXLabels = drawXLabels;
                DrawYLabels = drawYLabels;
            }

            public readonly int NumberOfDataPoints;
            public readonly int NumberOfXLabels;
            public readonly bool DrawXLabels;
            public readonly bool DrawYLabels;

            public List<string> Labels { get; private set; } = new List<string>();
            private int _count = 0;

            public void AddNewGraph(string title, Brush stroke = null)
            {
                if (Style == PriceIndexDrawObject.Styles.Candle)
                {
                    return;
                }
                else if (Style == PriceIndexDrawObject.Styles.Default)
                {
                    _defaultSeriesCollection.Add(new LineSeries{Values = new ChartValues<decimal>(),
                        Stroke = stroke,
                        Fill = Brushes.Transparent,
                        PointGeometrySize = 1,
                        StrokeThickness = 1.5,
                        Title = title,
                    });
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            public void AddDataPoint(PriceIndex pi, PriceIndex.DataPoint dataPoint)
            {

                var shouldDrawData = DrawXLabels && (_count % (NumberOfDataPoints / NumberOfXLabels) == 0 ||
                                     dataPoint.DateTime == pi.DataPoints.Last().DateTime);

                ++_count;
                if (DrawXLabels)
                {
                    Labels.Add(shouldDrawData ? dataPoint.DateTime.Date.ToString("d") : "");
                }
                if (Style == PriceIndexDrawObject.Styles.Candle)
                {
                    decimal currentValue = 0;
                    _lowTransparentSeries.Values.Add(dataPoint.PriceMin.Value - currentValue);
                    currentValue = dataPoint.PriceMin.Value;
                    var minOpenClose = Math.Min(dataPoint.PriceClose.Value, dataPoint.PriceOpen.Value);
                    _lowToOpenCloseMinSeries.Values.Add(minOpenClose - currentValue);
                    currentValue = minOpenClose;
                    var maxOpenClose = Math.Max(dataPoint.PriceClose.Value, dataPoint.PriceOpen.Value);
                    if (dataPoint.PriceOpen.Value >= dataPoint.PriceClose.Value)
                    {
                        _openCloseMinToMaxSeries_Negative.Values.Add(maxOpenClose - currentValue);
                        _openCloseMinToMaxSeries_Positive.Values.Add((decimal)0);
                    }
                    else
                    {
                        _openCloseMinToMaxSeries_Positive.Values.Add(maxOpenClose - currentValue);
                        _openCloseMinToMaxSeries_Negative.Values.Add((decimal)0);
                    }
                    currentValue = maxOpenClose;
                    _openCloseMaxToMaxSeries.Values.Add(dataPoint.PriceMax - currentValue);
                }
                else if (Style == PriceIndexDrawObject.Styles.Default)
                {
                    _defaultSeriesCollection.Last().Values.Add(dataPoint.Price.Value);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            StackedColumnSeries _lowTransparentSeries = new StackedColumnSeries
            {
                Title = "Low",
                Values = new ChartValues<decimal>(),
                Fill = Brushes.Transparent,
            };
            StackedColumnSeries _lowToOpenCloseMinSeries = new StackedColumnSeries
            {
                Title = "Low-OpenCloseLow",
                Values = new ChartValues<decimal>(),
                Fill = Brushes.SaddleBrown,
                Stroke = Brushes.Transparent,
                MaxColumnWidth = 2,
                ColumnPadding = 0,
            };
            StackedColumnSeries _openCloseMinToMaxSeries_Positive = new StackedColumnSeries
            {
                Title = "OpenCloseLow-High_Positive",
                Values = new ChartValues<decimal>(),
                Fill = Brushes.Green,
                ColumnPadding = 0,
            };
            StackedColumnSeries _openCloseMinToMaxSeries_Negative = new StackedColumnSeries
            {
                Title = "OpenCloseLow-High_Negative",
                Values = new ChartValues<decimal>(),
                Fill = Brushes.OrangeRed,
                ColumnPadding = 0,
            };
            StackedColumnSeries _openCloseMaxToMaxSeries = new StackedColumnSeries
            {
                Title = "OpenCloseHigh-High",
                Values = new ChartValues<decimal>(),
                Fill = Brushes.DarkOliveGreen,
                Stroke = Brushes.Transparent,
                MaxColumnWidth = 2,
                ColumnPadding = 0,
            };

            private SeriesCollection _defaultSeriesCollection = new SeriesCollection();

            public SeriesCollection SeriesCollection
            {
                get
                {
                    if (Style == PriceIndexDrawObject.Styles.Candle)
                    {
                        return new SeriesCollection
                        {
                            _lowTransparentSeries,
                            _lowToOpenCloseMinSeries,
                            _openCloseMinToMaxSeries_Positive,
                            _openCloseMinToMaxSeries_Negative,
                            _openCloseMaxToMaxSeries,
                        };
                    }
                    else if (Style == PriceIndexDrawObject.Styles.Default)
                    {
                        return _defaultSeriesCollection;
                    }
                    else
                    {

                        throw new NotImplementedException();
                    }
                    
                }

            }
        }

        public void Draw()
        {
            if (PriceIndexDrawObjects.Count == 0)
            {
                Chart.VisualElements.Clear();
                Chart.Visibility = Visibility.Hidden;
                return;
            }

            var drawer = new Drawer(PriceIndexDrawObjects.First().Style, PriceIndexDrawObjects.First().NumberOfDataPoints, PriceIndexDrawObjects.First().NumberOfXLabels, PriceIndexDrawObjects.First().DrawXLabels, PriceIndexDrawObjects.First().DrawXLabels);

            foreach (var PriceIndexDrawObject in PriceIndexDrawObjects)
            {
                drawer.AddNewGraph(title: PriceIndexDrawObject.Name, stroke: PriceIndexDrawObject.Stroke);
                var priceIndex = PriceIndexDrawObject.PriceIndex;
                if (priceIndex.DataPoints == null)
                {
                    continue;
                }
                var priceIndexFold = priceIndex.Fold(PriceIndexDrawObjects.First().NumberOfDataPoints);
                foreach (var dataPoint in priceIndexFold.DataPoints)
                {
                    if (!dataPoint.Price.HasValue)
                    {
                        continue;
                    }
                    drawer.AddDataPoint(priceIndexFold, dataPoint);
                }
            }
            if (PriceIndexDrawObjects.First().DrawXLabels)
            {
                XAML_XAxis.Labels = drawer.Labels.ToArray();
            }
            else
            {
                XAML_XAxis.Labels = new List<string>();
            }
            if (!PriceIndexDrawObjects.First().DrawYLabels)
            {
                XAML_YAxis.Labels = new List<string>();
            }
            Chart.Series = drawer.SeriesCollection;
            Chart.DataTooltip.Visibility = PriceIndexDrawObjects.First().Style == PriceIndexDrawObject.Styles.Candle ? Visibility.Hidden : Visibility.Visible;
            Chart.Update(true, true);
        }


       
    }

}