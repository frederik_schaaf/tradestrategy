﻿using System.Windows;

namespace Visualizer
{
    /// <summary>
    /// Interaction logic for StrategyComparerWindow.xaml
    /// </summary>
    public partial class StrategyComparerWindow : Window
    {
        public StrategyComparerWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        public string WindowTitle => StrategyComparer.Title;
    }
}
