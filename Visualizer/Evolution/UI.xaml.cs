﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using TradeStrategy.DataDefinition;
using TradeStrategy.Parser;
using TradeStrategy.Strategy;
using Unity;
using static TradeStrategy.Strategy.MassEvolver;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.MessageBox;
using Microsoft.WindowsAPICodePack.Dialogs;
using DataGrid = System.Windows.Controls.DataGrid;

namespace Visualizer.Evolution
{
    /// <summary>
    /// Interaction logic for EvolutionUI.xaml
    /// </summary>
    public partial class UI : Window
    {
        private static ObservableCollection<StrategiesDataGridRow> GetDefaultStrategies()
        {
            var result = new ObservableCollection<StrategiesDataGridRow>();
            foreach (var strat in TradeStrategy.Strategy.DefaultStrategyList.Strategies)
            {
                result.Add(StrategiesDataGridRow.FromStrategy(strat, isDefault: true));
            }
            return result;
        }

        public UI()
        {
            InitializeComponent();
            this.StrategiesDataGrid.ItemsSource = GetDefaultStrategies();
            this.PriceIndexDataGrid.ItemsSource = new ObservableCollection<PriceIndexDataGridRow>();
        }

        public ObservableCollection<PriceIndexDataGridRow> PriceIndexDataGridItems => (ObservableCollection<PriceIndexDataGridRow>) this.PriceIndexDataGrid.ItemsSource;

        public ObservableCollection<StrategiesDataGridRow> StrategiesDataGridItems => (ObservableCollection<StrategiesDataGridRow>)this.StrategiesDataGrid.ItemsSource;

        private void PriceIndexDataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "PriceIndexDrawObjects" || (string)e.Column.Header == "ResultStrategies" || (string)e.Column.Header == "Years" || (string)e.Column.Header == "AnnualROI")
            {
                e.Cancel = true;
            }
        }

        void OnProgress(object sender, ProgressEventArgs e)
        {
            this.ProgressBar.Dispatcher.Invoke(() =>
                {
                    ProgressBar.Value = e.Progress;
                }
            );
        }

        private List<IStrategy> FindBestAndWorstStrategies(List<Tuple<IStrategy, double>> candidates)
        {
            var bestWorstByType = new Dictionary<Type, Tuple<Tuple<IStrategy, double>, Tuple<IStrategy, double>>>();
            foreach (var candidate in candidates)
            {
                var strategy = candidate.Item1;
                var type = strategy.GetType();
                var annualRoi = candidate.Item2;

                if (!bestWorstByType.ContainsKey(type))
                {
                    bestWorstByType.Add(type, new Tuple<Tuple<IStrategy, double>, Tuple<IStrategy, double>>(new Tuple<IStrategy, double>(strategy, annualRoi), new Tuple<IStrategy, double>(strategy, annualRoi)));
                }
                else
                {
                    if (annualRoi > bestWorstByType[type].Item1.Item2)
                    {
                        bestWorstByType[type] = new Tuple<Tuple<IStrategy, double>, Tuple<IStrategy, double>>(new Tuple<IStrategy, double>(strategy, annualRoi), bestWorstByType[type].Item1);
                    }
                    else if (annualRoi < bestWorstByType[type].Item2.Item2)
                    {
                        bestWorstByType[type] = new Tuple<Tuple<IStrategy, double>, Tuple<IStrategy, double>>(bestWorstByType[type].Item1, new Tuple<IStrategy, double>(strategy, annualRoi));
                    }
                }
            }
            var result = new List<IStrategy>();
            foreach (var item in bestWorstByType)
            {
                result.Add(item.Value.Item1.Item1);
                if (!item.Value.Item1.Item1.Equals(item.Value.Item2.Item1))
                {
                    result.Add(item.Value.Item2.Item1);
                }
            }
            return result;
        }

        private void Evolve_OnClick(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;

            var priceIndecesToEvolve = new List<PriceIndex>();

            foreach (var item in this.PriceIndexDataGrid.ItemsSource)
            {
                var dgr = (PriceIndexDataGridRow)item;
                if (dgr.Evolve)
                {
                    priceIndecesToEvolve.Add(dgr.PriceIndexDrawObjects.First().PriceIndex);
                }
            }

            if (priceIndecesToEvolve.Count == 0)
            {
                this.IsEnabled = true;
                return;
            }

            var strategiesToEvolve = new List<MassEvolver.TypedStrategy>();

            foreach (var item in this.StrategiesDataGrid.ItemsSource)
            {

                var dgr = (StrategiesDataGridRow)item;
                if (dgr.IsDefault)
                {
                    strategiesToEvolve.Add(new MassEvolver.TypedStrategy
                    {
                        Strategy = dgr.Strategy,
                        IsDefault = dgr.IsDefault
                    });
                }
            }

            Progress += OnProgress;
            var nSteps = (int)this.NStepsSlider.Value;

            var t = new Thread(() =>
            {
                var massEvolverResult = MassEvolver.Evolve(priceIndecesToEvolve, strategiesToEvolve, nSteps);

                this.Dispatcher.Invoke(() =>
                {

                    ClearAll();

                    Progress -= OnProgress;
                    this.ProgressBar.Value = 0;

                    // Add Result Strategies to each PriceIndex if checked
                    foreach (var dgri in this.PriceIndexDataGridItems)
                    {
                        if (dgri.Evolve)
                        {
                            var resultStrategies = new ObservableCollection<IStrategy>();
                            foreach (var priceIndexResult in massEvolverResult.PriceIndexResults)
                            {
                                if (dgri.Name == priceIndexResult.Original.Name)
                                {
                                    foreach (var elem in priceIndexResult.StrategyResults)
                                    {
                                        resultStrategies.Add(elem.Strategy);
                                    }
                                }
                            }
                            dgri.ResultStrategies = resultStrategies;
                        }
                    }

                    var toAddCandidates = new List<StrategyResult>();
                    foreach (var sr in massEvolverResult.StrategyResults)
                    {
                        bool found = false;
                        foreach (var sdgri in StrategiesDataGridItems)
                        {
                            if (sdgri.Strategy.Name == sr.Strategy.Name)
                            {
                                sdgri.EvolverResult = sr;
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            toAddCandidates.Add(sr);
                        }
                    }

                    var toAdd = new List<StrategyResult>();

                    toAdd = toAddCandidates;

                    var allStrategies = new HashSet<Tuple<IStrategy, double>>();
                    foreach (var item in toAddCandidates)
                    {
                        allStrategies.Add(new Tuple<IStrategy, double>(item.Strategy, item.AverageAnnualROI));
                    }
                    foreach (var item in StrategiesDataGridItems)
                    {
                        allStrategies.Add(new Tuple<IStrategy, double>(item.Strategy, item.AvgAnnualROI.Value));
                    }
                    var bestWorstStrategies = FindBestAndWorstStrategies(allStrategies.ToList());
                    var toRemove = new List<StrategyResult>();
                    foreach (var sr in toAdd)
                    {
                        bool found = false;
                        foreach (var s in bestWorstStrategies)
                        {
                            if (sr.Strategy.Equals(s))
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            toRemove.Add(sr);
                        }
                    }
                    foreach (var r in toRemove)
                    {
                        toAdd.Remove(r);
                    }
                        

                    foreach (var sr in toAdd)
                    {
                        var sdgri = StrategiesDataGridRow.FromStrategy(
                            sr.Strategy,
                            false
                        );
                        sdgri.EvolverResult = sr;
                        StrategiesDataGridItems.Add(sdgri);
                    }
                    
                    this.IsEnabled = true;
                });

            });

            t.Start();
        }

        private void PriceIndex_RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var button = (Button) sender;
            var pidgr = (PriceIndexDataGridRow) button.DataContext;
            var collection = (ObservableCollection<PriceIndexDataGridRow>)this.PriceIndexDataGrid.ItemsSource;
            collection.Remove(pidgr);
        }

        List<IStrategy> getAllStrategiesForIndex(PriceIndex pi)
        {
            var result = new List<IStrategy>();
            foreach (var strategy in StrategiesDataGridItems)
            {
                if (!strategy.HasEvolverResult)
                {
                    var s = strategy.Strategy.Clone();
                    s.Apply(pi);
                    result.Add(s);
                }
                else
                {
                    foreach (var eResult in strategy.EvolverResult.Results)
                    {
                        if (eResult.OriginalPriceIndex.Name != pi.Name) continue;
                        result.Add(eResult.Strategy);
                    }
                }
            }
            return result;
        }

        private void PriceIndex_ViewButton_OnClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var pidgr = (PriceIndexDataGridRow)button.DataContext;
            var w = new StrategyComparerWindow();
            w.StrategyComparer.Title = pidgr.Name;
            
            w.StrategyComparer.ReferenceIndex = pidgr.PriceIndexDrawObjects.First().PriceIndex;
            var strategies = getAllStrategiesForIndex(pidgr.PriceIndexDrawObjects.First().PriceIndex);
            // ReSharper disable once AssignmentInConditionalExpression
            if (w.StrategyComparer.EnableCompareControl = strategies.Count != 0)
            {
                foreach (var strategy in strategies)
                {
                    w.StrategyComparer.CompareIndices.Add(strategy.GetResultPriceIndex().Value);
                }
            }
            w.StrategyComparer.NumberOfDataPoints = this.Checkbox_ShowMoreDataPoints.IsChecked == true ? NUMBER_OF_DATA_POINTS_MANY : NUMBER_OF_DATA_POINTS_FEW;
            w.StrategyComparer.Update();
            w.Show();
        }

        public void PriceIndex_Load(string path)
        {
            using (Stream stream = new FileStream(path, FileMode.Open))
            {
                var fn = Path.GetFileName(path);
                var splits = fn.Split('.');
                var name = splits[splits.Length - 2];
                PriceIndex_Load(stream, name);
            }
        }

        public void PriceIndex_Load(Stream stream, string name)
        {
            var priceIndexParser = TradeStrategy.Unity.UnityContainer.Resolve<IPriceIndexParser>();
            using (stream)
            {
                var priceIndex = priceIndexParser.Parse(stream).GetNormalized();
                priceIndex.Name = name;
                PriceIndexDataGridItems.Add(PriceIndexDataGridRow.FromPriceIndexControl
                (
                    new ObservableCollection<PriceIndexDrawObject>
                    {
                        new PriceIndexDrawObject
                        {
                            PriceIndex = priceIndex,
                            NumberOfDataPoints = 25,
                            DrawXLabels = false,
                            DrawYLabels = false
                        }
                    }
                ));
            }
        }

        public int PriceIndex_LoadDirectory(string path, int recursionLevel = 0)
        {
            int fileErrors = 0; 
            try
            {
                foreach (string f in Directory.GetFiles(path))
                {
                    try
                    {
                        PriceIndex_Load(f);
                    }
                    catch (Exception)
                    {
                        ++fileErrors;
                    }
                }
                foreach (string d in Directory.GetDirectories(path))
                {

                    fileErrors += PriceIndex_LoadDirectory(d, recursionLevel + 1);
                }
            }
            catch (System.Exception)
            {
                ++fileErrors;
            }
            if (recursionLevel == 0 && fileErrors > 0)
            {
                MessageBox.Show($"{fileErrors} File(s) have not been loaded.", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
            return fileErrors;
        }

        public int Strategy_LoadDirectory(string path, int recursionLevel = 0)
        {
            int fileErrors = 0;
            try
            {
                foreach (string f in Directory.GetFiles(path))
                {
                    try
                    {
                        var strat = Detail.LoadFromFile(f);
                        var found = false;
                        foreach (var sdgi in this.StrategiesDataGridItems)
                        {
                            if (sdgi.Name == strat.Name)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            this.StrategiesDataGridItems.Add(StrategiesDataGridRow.FromStrategy(strat, false));
                        }
                    }
                    catch (Exception)
                    {
                        ++fileErrors;
                    }
                }

                foreach (string d in Directory.GetDirectories(path))
                {
                    fileErrors += Strategy_LoadDirectory(d, recursionLevel + 1);
                }
            }
            catch (System.Exception)
            {
                ++fileErrors;
            }
            if (recursionLevel == 0 && fileErrors > 0)
            {
                MessageBox.Show($"{fileErrors} File(s) have not been loaded.", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
            return fileErrors;
        }

        private void MenuItem_PriceIndex_LoadFolder_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            CommonFileDialogResult result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
            {
                var path = dialog.FileName;
                try
                {
                    PriceIndex_LoadDirectory(path);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "PriceIndex_Load Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }

        private void MenuItem_Strategy_LoadFolder_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            CommonFileDialogResult result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
            {
                var path = dialog.FileName;
                try
                {
                    Strategy_LoadDirectory(path);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "PriceIndex_Load Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }

        private void MenuItem_PriceIndex_LoadFile_OnClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".csv",
                Filter = "csv Files (*.csv)|*.csv|All Files (*.*)|*.*"
            };
            var result = dlg.ShowDialog(this.Owner);
            if (result == true)
            {
                string fileName = dlg.FileName;
                try
                {
                    PriceIndex_Load(fileName);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "PriceIndex_Load Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }

        private void MenuItem_Strategy_LoadFile_OnClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".json",
                Filter = "json Files (*.json)|*.json|All Files (*.*)|*.*"
            };
            var result = dlg.ShowDialog(this.Owner);
            if (result == true)
            {
                var strat = Detail.LoadFromFile(dlg.FileName);
                this.StrategiesDataGridItems.Add(StrategiesDataGridRow.FromStrategy(strat, false));
            }
        }

        private void Strategy_SaveAll(string folder)
        {
            try
            {
                foreach (var sdgi in this.StrategiesDataGridItems)
                {
                    if (sdgi.IsDefault)
                    {
                        continue;
                    }
                    Detail.SaveToFile(sdgi.Strategy, Path.Combine(folder,$"{sdgi.Name}.json"));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show($"An Error occured: {e.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void MenuItem_Strategy_SaveAll_OnClick(object sender, RoutedEventArgs e)
        {
            var fbd = new FolderBrowserDialog();
            var result = fbd.ShowDialog();
            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            if (Directory.EnumerateFileSystemEntries(fbd.SelectedPath).Any())
            {
                MessageBox.Show("The directory must be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Strategy_SaveAll(fbd.SelectedPath);
        }

        private void StrategiesDataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if ((string)e.Column.Header == "AvgAnnualROI" || (string)e.Column.Header == "Strategy" || (string)e.Column.Header == "HasEvolverResult" || (string)e.Column.Header == "EvolverResult" || (string)e.Column.Header == "IsDefault" || (string)e.Column.Header == "InfoText")
            {
                e.Cancel = true;
            }
        }

        private void Strategy_RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var sdgr = (StrategiesDataGridRow)button.DataContext;
            var collection = (ObservableCollection<StrategiesDataGridRow>)this.StrategiesDataGrid.ItemsSource;
            collection.Remove(sdgr);
        }

        private void Strategy_SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var sdgr = (StrategiesDataGridRow)button.DataContext;
            var dlg = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".json",
                Filter = "json Files (*.json)|*.json|All Files (*.*)|*.*"
            };
            var result = dlg.ShowDialog(this.Owner);
            if (result == true)
            {
                Detail.SaveToFile(sdgr.Strategy, dlg.FileName);
            }
        }

        static readonly int NUMBER_OF_DATA_POINTS_FEW = 20;
        static readonly int NUMBER_OF_DATA_POINTS_MANY = 50;

        private void Strategy_ViewButton_OnClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var sdgr = (StrategiesDataGridRow)button.DataContext;
            var w = new StrategyComparerWindow();
            w.StrategyComparer.Title = sdgr.Name;
            var requiredIndeces = new Dictionary<string, bool>();
            foreach (var pidgi in this.PriceIndexDataGridItems)
            {
                requiredIndeces.Add(pidgi.Name, false);
            }
            if (sdgr.EvolverResult?.Results != null)
            {
                foreach (var eResult in sdgr.EvolverResult.Results.ToArray())
                {
                    string match = null;
                    foreach (var reqI in requiredIndeces)
                    {
                        var reqName = reqI.Key + " | " + sdgr.Name;
                        if (reqName == eResult.Strategy.GetResultPriceIndex().Value.Name)
                        {
                            match = reqI.Key;
                            break;
                        }
                    }
                    if (match != null)
                    {
                        requiredIndeces[match] = true;
                        w.StrategyComparer.CompareIndices.Add(eResult.Strategy.GetResultPriceIndex().Value);
                    }
                }
            }
            foreach (var reqI in requiredIndeces)
            {
                if (reqI.Value)
                {
                    continue;
                }
                foreach (var pidgi in this.PriceIndexDataGridItems)
                {
                    if (pidgi.Name == reqI.Key)
                    {
                        var fullName = reqI.Key + " | " + sdgr.Name;
                        var s = sdgr.Strategy.Clone();
                        var pi = s.Apply(pidgi.PriceIndexDrawObjects.First().PriceIndex);
                        pi.Name = fullName;
                        w.StrategyComparer.CompareIndices.Add(pi);
                    }
                }
            }
            w.StrategyComparer.NumberOfDataPoints = this.Checkbox_ShowMoreDataPoints.IsChecked == true ? NUMBER_OF_DATA_POINTS_MANY : NUMBER_OF_DATA_POINTS_FEW;
            w.StrategyComparer.Update();
            w.Show();
        }

        private void StrategiesDataGrid_OnAutoGeneratedColumns(object sender, EventArgs e)
        {
            var grid = (DataGrid)sender;
            var SORT_LIST = new[]
            {
                "Actions",
                "Evolve",
                "Name",
            };

            foreach (var item in grid.Columns)
            {
                var i = 0;
                foreach (var sort_string in SORT_LIST)
                {
                    if ((string)item.Header == sort_string)
                    {
                        item.DisplayIndex = i;
                    }
                    ++i;
                }
            }

        }

        private void PriceIndexDataGrid_OnAutoGeneratedColumns(object sender, EventArgs e)
        {
 
            var grid = (DataGrid)sender;
            var SORT_LIST = new[]
            {
                "Actions",
                "Evolve",
                "Preview",
                "Name",
                "Years",
                "AnnualROI",
                "ResultStrategies"
            };

            foreach (var item in grid.Columns)
            {
                var i = 0;
                foreach (var sort_string in SORT_LIST)
                {
                    if ((string)item.Header == sort_string)
                    {
                        item.DisplayIndex = i;
                    }
                    ++i;
                }
            }
        }

        private void PriceIndexClearAllButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Remove all price indeces?", "Confirm", MessageBoxButton.OKCancel,
                    MessageBoxImage.Question) != MessageBoxResult.OK)
            {
                return;
            }
            var collection = (ObservableCollection<PriceIndexDataGridRow>)this.PriceIndexDataGrid.ItemsSource;
            collection.Clear();
        }

        private void ClearAll()
        {
            var collection = (ObservableCollection<StrategiesDataGridRow>)this.StrategiesDataGrid.ItemsSource;
            var toRemove = new List<StrategiesDataGridRow>();
            foreach (var item in collection)
            {
                if (!item.IsDefault)
                {
                    toRemove.Add(item);
                }

            }
            foreach (var item in toRemove)
            {
                collection.Remove(item);
            }
        }

        private void StrategiesClearAllButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Remove all strategies?", "Confirm", MessageBoxButton.OKCancel,
                    MessageBoxImage.Question) != MessageBoxResult.OK)
            {
                return;
            }
            ClearAll();
        }
    }
}
