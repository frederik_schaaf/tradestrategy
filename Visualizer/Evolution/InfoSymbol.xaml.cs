﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace Visualizer.Evolution
{
    [ContentProperty(nameof(PopupContent))]
    public partial class InfoSymbol : UserControl
    {
        public InfoSymbol()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty PopupContentProperty = DependencyProperty.Register(nameof(PopupContent), typeof(object), typeof(InfoSymbol),
            null);

        public object PopupContent
        {
            get { return GetValue(PopupContentProperty); }
            set { SetValue(PopupContentProperty, value); }
        }
    }
}