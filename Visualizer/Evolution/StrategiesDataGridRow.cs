﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeStrategy.Strategy;

namespace Visualizer.Evolution
{
    public class StrategiesDataGridRow : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public static StrategiesDataGridRow FromStrategy(IStrategy strategy, bool isDefault)
        {
            return new StrategiesDataGridRow
            {
                Strategy = strategy,
                IsDefault = isDefault
            };
        }

        public IStrategy Strategy { get; private set; }
        public string Name => Strategy.Name;
        public bool IsDefault { get; private set; }
        public string InfoText => Strategy.InfoText;
        public double? AvgAnnualROI => _evolverResult?.AverageAnnualROI;
        private MassEvolver.StrategyResult _evolverResult;

        public bool HasEvolverResult
        {
            get
            {
                return _evolverResult != null;
            }
        }
        

        public MassEvolver.StrategyResult EvolverResult
        {
            get => _evolverResult;
            set
            {
                _evolverResult = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("EvolverResult"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AvgAnnualROI"));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("HasEvolverResult"));
            }
        }

    }
}
