﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeStrategy.Strategy;

namespace Visualizer.Evolution
{
    public class PriceIndexDataGridRow : INotifyPropertyChanged
    {
        private ObservableCollection<PriceIndexDrawObject> _priceIndexDrawObjects;
        private string _name;
        private bool _evolve = true;
        private ObservableCollection<IStrategy> _resultStrategies;

        public static PriceIndexDataGridRow FromPriceIndexControl(ObservableCollection<PriceIndexDrawObject> priceIndexDrawObjects)
        {
            return new PriceIndexDataGridRow
            {
                PriceIndexDrawObjects = priceIndexDrawObjects,
                Name = priceIndexDrawObjects.First().Name,
            };
        }

        public ObservableCollection<PriceIndexDrawObject> PriceIndexDrawObjects
        {
            get => _priceIndexDrawObjects;
            set
            {
                _priceIndexDrawObjects = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PriceIndexDrawObjects"));
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }

        public double? Years => (double?)(_priceIndexDrawObjects?.First()?.PriceIndex.TimeSpan?.Days)/365;

        public double? AnnualROI => _priceIndexDrawObjects?.First()?.PriceIndex.AnnualROI;

        public bool Evolve
        {
            get => _evolve;
            set
            {
                _evolve = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Evolve"));
            }
        }

        public ObservableCollection<IStrategy> ResultStrategies
        {
            get => _resultStrategies;
            set
            {
                _resultStrategies = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ResultStrategies"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}
