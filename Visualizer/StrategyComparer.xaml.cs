﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TradeStrategy.DataDefinition;
using Brush = System.Drawing.Brush;
using Color = System.Drawing.Color;

namespace Visualizer
{
    /// <summary>
    /// Interaction logic for StrategyComparer.xaml
    /// </summary>
    public partial class StrategyComparer : UserControl
    {
        public StrategyComparer()
        {
            InitializeComponent();
            DataContext = this;
        }

        public int NumberOfDataPoints { get; set; } = 20;

        public bool EnableCompareControl { get; set; } = true;

        public string Title { get; set; }

        public PriceIndex ReferenceIndex { get; set; }

        public ObservableCollection<PriceIndex> CompareIndices { get; set; } = new ObservableCollection<PriceIndex>();

        public static readonly List<System.Windows.Media.Brush> CompareStrokes = new List<System.Windows.Media.Brush>
        {
            Brushes.Red,
            Brushes.Blue,
            Brushes.Brown,
            Brushes.Purple,
            Brushes.Gray,
            Brushes.Green,
            Brushes.BurlyWood,
            Brushes.Chartreuse,
            Brushes.Coral,
            Brushes.DarkOliveGreen,
            Brushes.DarkCyan,
            Brushes.MediumPurple,
            Brushes.Yellow,
            Brushes.OrangeRed,
        };

        public ObservableCollection<PriceIndexDrawObject> ComparePidos
        {
            get
            {
                if (!EnableCompareControl)
                {
                    return new ObservableCollection<PriceIndexDrawObject>();
                }
                var result = new ObservableCollection<PriceIndexDrawObject>();
                result.Add(new PriceIndexDrawObject
                {
                    PriceIndex = ReferenceIndex,
                    Style = PriceIndexDrawObject.Styles.Default,
                    Stroke = Brushes.Black,
                    NumberOfDataPoints = this.NumberOfDataPoints,
                });
                var strokeEnumerator = CompareStrokes.GetEnumerator();
                foreach (var compareIndex in CompareIndices)
                {
                    if (!strokeEnumerator.MoveNext())
                    {
                        strokeEnumerator = CompareStrokes.GetEnumerator();
                    }
                    
                    result.Add(new PriceIndexDrawObject
                    {
                        PriceIndex = compareIndex,
                        Style = PriceIndexDrawObject.Styles.Default,
                        Stroke = strokeEnumerator.Current,
                        NumberOfDataPoints = this.NumberOfDataPoints,
                    });
                }
                return result;
            }
        }

        public SummaryRow ReferenceSummary => SummaryRow.CreateFrom(ReferenceIndex);

        public static string Percent(double? val)
        {
            if (val == null)
            {
                return "-";
            }
            return $"{val:P1}";
        }

        public List<SummaryRow> CompareSummary
        {
            get
            {
                if (!EnableCompareControl)
                {
                    return new List<SummaryRow>();
                }
                var result = new List<SummaryRow>();
                if (ReferenceSummary.Name != null)
                {
                    result.Add(ReferenceSummary);
                }
                foreach (var compareIndex in CompareIndices)
                {
                    result.Add( SummaryRow.CreateFrom(compareIndex, ReferenceIndex));
                }
                return result;
            }
        }

        public class SummaryRow
        {
            public static SummaryRow CreateFrom(PriceIndex pi, PriceIndex? reference = null)
            {
                return new SummaryRow
                {
                    Name = pi.Name,
                    ROI = pi.ROI,
                    AnnualROI = pi.AnnualROI,
                    DiffAnnualROI = pi.AnnualROI-reference?.AnnualROI,
                };
            }

            public string Name { get; set; }
            public double? ROI { get; set; }
            public double? AnnualROI { get; set; }
            public double? DiffAnnualROI { get; set; }
        }

        private List<RowDefinition> _rowDefinitionCollection;
        private void UpdateRowDefinitions(int n)
        {
            if (_rowDefinitionCollection == null)
            {
                _rowDefinitionCollection = new List<RowDefinition>();
                foreach (var item in this.DisplayGrid.RowDefinitions)
                {
                    _rowDefinitionCollection.Add(item);
                }
            }

            if (this.DisplayGrid.RowDefinitions.Count > n)
            {
                for (int i = n; i < this.DisplayGrid.RowDefinitions.Count; ++i)
                {
                    this.DisplayGrid.RowDefinitions.RemoveAt(i);
                }
            }
            if (this.DisplayGrid.RowDefinitions.Count < n)
            {
                for (int i = this.DisplayGrid.RowDefinitions.Count; i < n; ++i)
                {
                    this.DisplayGrid.RowDefinitions.Add(_rowDefinitionCollection[i]);
                }
            }
        }

        private bool hasData(ObservableCollection<PriceIndexDrawObject> pido, int index = 0)
        {
            return pido?.Count > index && pido[index].PriceIndex.DataPoints?.Count > 0;
        }

        public void Update()
        {
            this.PriceIndexControlMain.PriceIndexDrawObjects = new ObservableCollection<PriceIndexDrawObject>{ new PriceIndexDrawObject{Style = PriceIndexDrawObject.Styles.Candle, PriceIndex = ReferenceIndex, NumberOfDataPoints = this.NumberOfDataPoints} };
            this.PriceIndexControlMain.Draw();
            this.PriceIndexControlCompare.PriceIndexDrawObjects = ComparePidos;
            this.PriceIndexControlCompare.Draw();
            this.SummaryMain.ItemsSource = new List<SummaryRow> {ReferenceSummary};
            var hasReferenceData = hasData(this.PriceIndexControlMain.PriceIndexDrawObjects);
            var hasCompareData = hasData(this.PriceIndexControlCompare.PriceIndexDrawObjects, 1);
            this.SummaryCompare.ItemsSource = hasCompareData ? CompareSummary : hasReferenceData ? new List<SummaryRow> { ReferenceSummary } : null;
            int rowCount = 0;
            rowCount += hasReferenceData ? 1 : 0;
            rowCount += hasCompareData ? 1 : 0;
            UpdateRowDefinitions(rowCount);
        }

        private void SummaryCompare_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyType == typeof(System.Double?))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "P2";
                (e.Column as DataGridTextColumn).CanUserSort = true;
            }
        }
    }
}
