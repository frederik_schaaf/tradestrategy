﻿using System.Linq;
using System.Reflection;
using System.Threading;
using NUnit.Framework;
using TradeStrategy.DataDefinition;
using TradeStrategy.Parser;
using Unity;
using Visualizer;

namespace TradeStrategy.Test
{
    [TestFixture]
    internal class StrategyComparer
    {
        [Test]
        [Explicit]
        public void Test()
        {
            var nItems = 0;
            var t = new Thread(() =>
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceNames = assembly.GetManifestResourceNames();
                var w = new StrategyComparerWindow();

                var MAX_ITEMS = 10;
                PriceIndex? referenceIndex = null;

                foreach (var resourceName in resourceNames)
                {
                    if (!resourceName.ToLower().EndsWith(".csv"))
                        continue;

                    var splits = resourceName.Split('.');
                    var title = splits[splits.Length - 2];

                    using (var stream = assembly.GetManifestResourceStream(resourceName))
                    {
                        var priceIndexParser = Unity.UnityContainer.Resolve<IPriceIndexParser>();
                        var priceIndex = priceIndexParser.Parse(stream);
                        priceIndex.Name = title;
                        if (!referenceIndex.HasValue)
                        {
                            referenceIndex = priceIndex;
                            w.StrategyComparer.ReferenceIndex = referenceIndex.Value;
                        }
                        else
                        {
                            w.StrategyComparer.CompareIndices.Add(priceIndex);
                        }
                        
                    }
                    if (++nItems == MAX_ITEMS) break;
                }
                w.StrategyComparer.Update();
                w.ShowDialog();
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            Assert.That(nItems, Is.GreaterThan(0));
        }
    }
}