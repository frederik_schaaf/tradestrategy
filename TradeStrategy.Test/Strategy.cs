﻿using System.Linq;
using System.Reflection;
using System.Threading;
using NUnit.Framework;
using TradeStrategy.DataDefinition;
using TradeStrategy.Parser;
using TradeStrategy.Strategy;
using Unity;
using Visualizer;

namespace TradeStrategy.Test
{
    [TestFixture]
    internal class Strategy
    {
        [Test]
        [Explicit]
        public void Test()
        {
            var nItems = 0;
            var t = new Thread(() =>
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceNames = assembly.GetManifestResourceNames();
                var w = new StrategyComparerWindow();

                var MAX_ITEMS = 1;

                foreach (var resourceName in resourceNames)
                {
                    if (!resourceName.ToLower().EndsWith(".csv"))
                        continue;

                    if (!resourceName.Contains("DGD"))
                        continue;

                    var splits = resourceName.Split('.');
                    var title = splits[splits.Length - 2];

                    using (var stream = assembly.GetManifestResourceStream(resourceName))
                    {
                        var priceIndexParser = Unity.UnityContainer.Resolve<IPriceIndexParser>();
                        var priceIndex = priceIndexParser.Parse(stream);
                        priceIndex = priceIndex.GetNormalized();
                        priceIndex.Name = title;
                        w.StrategyComparer.ReferenceIndex = priceIndex;
                        
                        for (int ib = 0; ib < 2; ++ib)
                        {
                            for (int ss = 0; ss < 4; ++ss)
                            {
                                for (int pa = 0; pa <= 1; ++pa)
                                { 
                                    var preserveAmounts = pa != 0;
                                    var initialBuy = 0.25 + (double) ib / 4;
                                    var stepSize = 0.1 + (double) ss / 2;
                                    var s = new Strategy_TradeOnThreshold(new Strategy_TradeOnThreshold.Settings
                                    {
                                        InitialBuy = initialBuy,
                                        StepSize = stepSize,
                                        PreserveAmounts = preserveAmounts,
                                    });

                                    var strategyIndex = s.Apply(priceIndex);
                                    strategyIndex.Name =
                                        $"TradeOnThreshold InitialBuy={initialBuy}, StepSize={stepSize}, PreserveAmounts={preserveAmounts}";
                                    w.StrategyComparer.CompareIndices.Add(strategyIndex);
                                }
                            }
                        }

                    }
                    if (++nItems == MAX_ITEMS) break;
                }
                w.StrategyComparer.Update();
                w.ShowDialog();
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            Assert.That(nItems, Is.GreaterThan(0));
        }
    }
}