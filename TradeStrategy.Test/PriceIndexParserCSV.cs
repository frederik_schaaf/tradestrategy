﻿using System;
using System.IO;
using System.Reflection;
using System.Resources;
using NUnit.Framework;
using TradeStrategy.DataDefinition;
using TradeStrategy.Parser;
using Unity;

namespace TradeStrategy.Test
{
    [TestFixture]
    public class PriceIndexParserCSV
    {

        public void PerformTest(Stream fileStream)
        {
            var priceIndexParser = Unity.UnityContainer.Resolve<IPriceIndexParser>();
            var priceIndex = priceIndexParser.Parse(fileStream);
            Assert.That(priceIndex.DataPoints?.Count, Is.GreaterThan(0));
            var lastDate = DateTime.MinValue;
            foreach (var dataPoint in priceIndex.DataPoints)
            {
                Assert.That(dataPoint.DateTime, Is.GreaterThan(lastDate));
                lastDate = dataPoint.DateTime;
            }
        }

        [Test]
        public void TestAllCSV()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceNames = assembly.GetManifestResourceNames();
            var totalFilesTested = 0;
            foreach (var resourceName in resourceNames)
            {
                if (!resourceName.ToLower().EndsWith(".csv"))
                {
                    continue;
                }
                ++totalFilesTested;
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                {
                     PerformTest(stream);
                }
            }
            Assert.That(totalFilesTested,Is.GreaterThan(0));
        }
    }
}
