﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using TradeStrategy.Parser;
using Unity;
using Visualizer;
using Visualizer.Evolution;

namespace TradeStrategy.Test
{
    class EvolutionUI
    {
        [Test]
        [Explicit]
        public void Test()
        {
            var t = new Thread(() =>
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceNames = assembly.GetManifestResourceNames();
                var w = new UI();
                foreach (var resourceName in resourceNames)
                {
                    if (!resourceName.ToLower().EndsWith(".csv"))
                    {
                        continue;
                    }
                    using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                    {
                        var splits = resourceName.Split('.');
                        var name = splits[splits.Length - 2];
                        w.PriceIndex_Load(stream, name);
                    }
                }
                w.ShowDialog();
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
        }
    }
}
