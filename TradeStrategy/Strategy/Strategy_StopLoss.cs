﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using TradeStrategy.DataDefinition;

namespace TradeStrategy.Strategy
{
    public class Strategy_StopLoss_Evolver : IEvolver<IStrategy>
    {
        public Strategy_StopLoss_Evolver(int nSteps)
        {
            NSteps = nSteps;
        }

        public int NSteps { get; private set; }
        private int currentStep;

        public Strategy_StopLoss.Settings GetFromNumber(int value)
        {
            if (value < 2)
            {
                return Strategy_StopLoss.Settings.Default;
            }
            var list = new List<MapNumberToParams.Param>
            {
                new MapNumberToParams.Param
                {
                    Min = 1,
                    Max = (int) Math.Pow(NSteps-1, 0.5),
                },
                new MapNumberToParams.Param
                {
                    Min = 0,
                    Max = 1,
                }
            };
            var result = MapNumberToParams.Map(value-1, list);
            return new Strategy_StopLoss.Settings
            {
                ThresholdSize = (double)result[0] / Math.Pow(list[0].Max, 0.5),
                Adapt = result[1] == 1,
            };
        }

        public IStrategy Current()
        {
            return currentStep > NSteps ? null : new Strategy_StopLoss(GetFromNumber(currentStep));
        }

        public IStrategy Next()
        {
            ++currentStep;
            return Current();
        }
    }

    public class Strategy_StopLoss : IStrategy
    {
        public string InfoText => "Buy in immediately, sell everything when price falls below threshold.";

        public struct Settings
        {
            public double ThresholdSize;
            public bool Adapt;
            public static Settings Default => new Settings
            {
                ThresholdSize = 0.2,
                Adapt = true
            };
        }

        public IStrategy Clone()
        {
            return new Strategy_StopLoss(Setting);
        }

        public bool Equals(IStrategy strategy)
        {
            return Name == strategy.Name;
        }

        public IEvolver<IStrategy> CreateEvolver(int nSteps)
        {
            return new Strategy_StopLoss_Evolver(nSteps);
        }

        public Strategy_StopLoss(Settings setting)
        {
            Setting = setting;
        }

        public readonly Settings Setting;

        public string Name => $"StopLoss [ThresholdSize={Setting.ThresholdSize:0.00};Adapt={Setting.Adapt}]";

        private PriceIndex? Result;
        private DateTime _dateTime;
        private double _shares = 0;
        private double _funds = 1;
        private double _price = 1;
        private double? _priceHigh;
        private double _value => _funds + _shares * _price;

        private Stack<Transaction> _transactionStack = new Stack<Transaction>();
        private List<Transaction> _transactionList = new List<Transaction>();

        public List<Transaction> GetTransactions()
        {
            return _transactionList;
        }

        private void buy(double funds, double price, bool saveToStack = true)
        {
            var fundsOld = _funds;
            var sharesOld = _shares;
            _shares += funds / price;
            _funds -= funds;
            var t = new Transaction
            {
                DateTime = _dateTime,
                Price = price,
                Value = price * _shares + _funds,
                FundsOld = fundsOld,
                FundsNew = _funds,
                SharesOld = sharesOld,
                SharesNew = _shares,
            };
            _transactionList.Add(t);
            if (saveToStack)
            {
                if (_transactionStack.Count == 0 || _transactionStack.Peek().IsPurchase)
                {
                    _transactionStack.Push(t);
                }
                else
                {
                    _transactionStack.Pop();
                }
            }
        }

        private void sell(double shares, double price, bool saveToStack = true)
        {
            var fundsOld = _funds;
            var sharesOld = _shares;
            _shares -= shares;
            _funds += shares * price;
            var t = new Transaction
            {
                DateTime = _dateTime,
                Price = price,
                Value = price * _shares + _funds,
                FundsOld = fundsOld,
                FundsNew = _funds,
                SharesOld = sharesOld,
                SharesNew = _shares,
            };
            _transactionList.Add(t);
            if (saveToStack)
            {
                if (_transactionStack.Count == 0 || _transactionStack.Peek().IsSale)
                {
                    _transactionStack.Push(t);
                }
                else
                {
                    _transactionStack.Pop();
                }
            }
        }

        private double? getThreshold()
        {
            if (!_priceHigh.HasValue) return null;
            return _priceHigh.Value / (1 + Setting.ThresholdSize);
        }
        private PriceIndex? _original;

        public PriceIndex? GetOriginalPriceIndex()
        {
            return _original;
        }

        public static decimal PRICE_MINIMUM = (decimal)0.0000001;

        public PriceIndex Apply(PriceIndex original)
        {
            if (Result != null)
            {
                return Result.Value;
            }
            _original = original;
            PriceIndex piNormalized = original.GetNormalized();
            _dateTime = piNormalized.DataPoints.First().DateTime;
            buy(1, 1, saveToStack:false);

            var resultIndex = new PriceIndex();
            resultIndex.Name = _original.Value.Name;
            resultIndex.DataPoints = new List<PriceIndex.DataPoint>();
            bool isFirst = true;

            foreach (var dataPoint in piNormalized.DataPoints)
            {
                if (!dataPoint.Price.HasValue || dataPoint.PriceMin < PRICE_MINIMUM)
                {
                    continue;
                }
                _dateTime = dataPoint.DateTime;
                if (!_priceHigh.HasValue || (this.Setting.Adapt && _priceHigh.HasValue && (double)dataPoint.PriceMax.Value > _priceHigh.Value))
                {
                    _priceHigh = (double) dataPoint.PriceMax.Value;
                }
                
                bool stopLoss = !isFirst && (double) dataPoint.PriceMin.Value <= getThreshold();
                if (stopLoss)
                {
                    var value = getThreshold().Value;
                    sell(1, value);
                    resultIndex.DataPoints.Add(new PriceIndex.DataPoint(dataPoint.DateTime, (decimal)value, null, null, null, null));
                    break;
                }
                else
                {
                    resultIndex.DataPoints.Add(new PriceIndex.DataPoint(dataPoint.DateTime, dataPoint.Price.Value, null, null, null, null));
                }
                isFirst = false;
            }

            resultIndex.Name = resultIndex.Name + " | " + Name;
            Result = resultIndex;
            return resultIndex;
        }

        public PriceIndex? GetResultPriceIndex()
        {
            return Result;
        }
    }
}
