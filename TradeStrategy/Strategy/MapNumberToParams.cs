﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeStrategy.Strategy
{
    static class MapNumberToParams
    {
        public struct Param
        {
            public int Min;
            public int Max;
            public int Range => Max - Min + 1;
        }

        public static List<int> Map(int value, List<Param> parameters)
        {
            var result = new List<int>();
            foreach (var parameter in parameters)
            {
                result.Add(value % parameter.Range + parameter.Min);
                value /= parameter.Range;
            }
            return result;
        }
    }
}
