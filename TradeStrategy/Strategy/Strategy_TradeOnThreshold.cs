﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using TradeStrategy.DataDefinition;

namespace TradeStrategy.Strategy
{
    public class Strategy_TradeOnThreshold_Evolver : IEvolver<IStrategy>
    {
        public Strategy_TradeOnThreshold_Evolver(int nSteps)
        {
            NSteps = nSteps;
        }

        public int NSteps { get; private set; }
        private int currentStep;

        public Strategy_TradeOnThreshold.Settings GetFromNumber(int value)
        {
            if (value < 2)
            {
                return Strategy_TradeOnThreshold.Settings.Default;
            }
            var list = new List<MapNumberToParams.Param>
            {
                new MapNumberToParams.Param
                {
                    Min = 1,
                    Max = (int) Math.Pow(NSteps - 1, (double) 1 / 3),
                },
                new MapNumberToParams.Param
                {
                    Min = 1,
                    Max = (int) Math.Pow(NSteps - 1, (double) 1 / 3),
                },
                new MapNumberToParams.Param
                {
                    Min = 0,
                    Max = 1,
                }
            };
            var result = MapNumberToParams.Map(value - 1, list);
            return new Strategy_TradeOnThreshold.Settings
            {
                InitialBuy = (double) result[0] / (list[0].Max + 1),
                StepSize = (double) result[1] / Math.Pow(list[1].Max, 0.5),
                PreserveAmounts = result[2] == 1,
            };
        }

        public IStrategy Current()
        {
            return currentStep > NSteps ? null : new Strategy_TradeOnThreshold(GetFromNumber(currentStep));
        }

        public IStrategy Next()
        {
            ++currentStep;
            return Current();
        }
    }

    public class Strategy_TradeOnThreshold : IStrategy
    {
        public string InfoText =>
            $"Buy in with [Desired Investment] * {Setting.InitialBuy:0.00}, then sell everytime the price rises above the next threshold, and buy everytime the price falls below the next threshold. " +
            $"\n\nTo calculate the thresholds, start at 1 and then choose them such that two neighbors have the quotient: ({(1 + Setting.StepSize):0.00})." +
            $"\n [ ... ; {Math.Pow(1 / (1 + Setting.StepSize), 3):0.00} ; {Math.Pow(1 / (1 + Setting.StepSize), 2):0.00} ; {1 / (1 + Setting.StepSize):0.00} ; 1 ; {(1 + Setting.StepSize):0.00} ; {Math.Pow((1 + Setting.StepSize), 2):0.00} ; {Math.Pow((1 + Setting.StepSize), 3):0.00} ; ... ]" +
            $"\n\nTo calculate the buy size, use the following formula: [Remaining funds] * { (1 - 1 / (1 + Setting.StepSize)):0.00}" +
            $"\n\nTo calculate the sell size, use the following formula: [Remaining shares] * { (1 - 1 / (1 + Setting.StepSize)):0.00}" +
            $"\n\nTypically, you would do the initial buy, and then create buying and selling orders at the price thresholds. Everytime an order executes, create one or two new orders for the neighboring thresholds.";


        public IStrategy Clone()
        {
            return new Strategy_TradeOnThreshold(Setting);
        }

        public struct Settings
        {
            public double InitialBuy;
            public double StepSize;
            public bool PreserveAmounts;
            public static Settings Default => new Settings
            {
                InitialBuy = 0.5,
                StepSize = 0.5,
                PreserveAmounts = true
            };
        }

        public bool Equals(IStrategy strategy)
        {
            return Name == strategy.Name;
        }

        public IEvolver<IStrategy> CreateEvolver(int nSteps)
        {
            return new Strategy_TradeOnThreshold_Evolver(nSteps);
        }

        public Strategy_TradeOnThreshold(Settings setting)
        {
            Setting = setting;
        }

        public readonly Settings Setting;

        public string Name => $"TradeOnThreshold [InitialBuy={Setting.InitialBuy:0.00};StepSize={Setting.StepSize:0.00};PreserveAmounts={Setting.PreserveAmounts}]";

        private PriceIndex? Result;
        private DateTime _dateTime;
        private double _shares = 0;
        private double _funds = 1;
        private double _price = 1;
        private double _lastTransactionPrice;
        private double _value => _funds + _shares * _price;

        private Stack<Transaction> _transactionStack = new Stack<Transaction>();
        private List<Transaction> _transactionList = new List<Transaction>();

        public List<Transaction> GetTransactions()
        {
            return _transactionList;
        }

        private void buy(double funds, double price, bool saveToStack = true)
        {
            var fundsOld = _funds;
            var sharesOld = _shares;
            _shares += funds / price;
            _funds -= funds;
            _lastTransactionPrice = price;
            var t = new Transaction
            {
                DateTime = _dateTime,
                Price = price,
                Value = price * _shares + _funds,
                FundsOld = fundsOld,
                FundsNew = _funds,
                SharesOld = sharesOld,
                SharesNew = _shares,
            };
            _transactionList.Add(t);
            if (saveToStack)
            {
                if (_transactionStack.Count == 0 || _transactionStack.Peek().IsPurchase)
                {
                    _transactionStack.Push(t);
                }
                else
                {
                    _transactionStack.Pop();
                }
            }
        }

        private void sell(double shares, double price, bool saveToStack = true)
        {
            var fundsOld = _funds;
            var sharesOld = _shares;
            _shares -= shares;
            _funds += shares * price;
            _lastTransactionPrice = price;
            var t = new Transaction
            {
                DateTime = _dateTime,
                Price = price,
                Value = price * _shares + _funds,
                FundsOld = fundsOld,
                FundsNew = _funds,
                SharesOld = sharesOld,
                SharesNew = _shares,
            };
            _transactionList.Add(t);
            if (saveToStack)
            {
                if (_transactionStack.Count == 0 || _transactionStack.Peek().IsSale)
                {
                    _transactionStack.Push(t);
                }
                else
                {
                    _transactionStack.Pop();
                }
            }
        }

        private double getNextBuyPrice()
        {
            return _lastTransactionPrice / (1 + Setting.StepSize);
        }

        private double getNextSellPrice()
        {
            return _lastTransactionPrice * (1 + Setting.StepSize);
        }

        private double getNextSellShareSize()
        {
            if (Setting.PreserveAmounts && _transactionStack.Count > 0 && _transactionStack.Peek().IsPurchase)
            {
                return _transactionStack.Peek().SharesBought;
            }
            else
            {
                return _shares * (1 - 1 / (1 + Setting.StepSize));
            }
        }

        private double getNextBuyFundsSize()
        {
            if (Setting.PreserveAmounts && _transactionStack.Count > 0 && _transactionStack.Peek().IsSale)
            {
                return _transactionStack.Peek().FundsReceived;
            }
            else
            {
                return _funds * (1 - 1 / (1 + Setting.StepSize));
            }
        }

        private void applyTriggers(double targetPrice)
        {
            var sellPrice = getNextSellPrice();
            var buyPrice = getNextBuyPrice();
            if (targetPrice > sellPrice)
            {
                var sellSharesSize = getNextSellShareSize();
                sell(sellSharesSize, sellPrice);
                applyTriggers(targetPrice);
            }
            else if (targetPrice < buyPrice)
            {
                var buyFundsSize = getNextBuyFundsSize();
                buy(buyFundsSize, buyPrice);
                applyTriggers(targetPrice);
            }
        }


        private PriceIndex? _original;

        public PriceIndex? GetOriginalPriceIndex()
        {
            return _original;
        }

        public static decimal PRICE_MINIMUM = (decimal)0.0000001;

        public PriceIndex Apply(PriceIndex original)
        {
            if (Result != null)
            {
                return Result.Value;
            }
            _original = original;
            PriceIndex piNormalized = original.GetNormalized();
            _dateTime = piNormalized.DataPoints.First().DateTime;
            buy(Setting.InitialBuy, 1, saveToStack:false);

            var resultIndex = new PriceIndex();
            resultIndex.Name = _original.Value.Name;
            resultIndex.DataPoints = new List<PriceIndex.DataPoint>();

            foreach (var dataPoint in piNormalized.DataPoints)
            {
                if (!dataPoint.Price.HasValue || dataPoint.PriceMin < PRICE_MINIMUM)
                {
                    continue;
                }
                _dateTime = dataPoint.DateTime;
                applyTriggers((double)dataPoint.PriceMin.Value);
                applyTriggers((double)dataPoint.PriceMax.Value);
                _price = (double)dataPoint.Price.Value;
                var resultDataPoint = new PriceIndex.DataPoint(dataPoint.DateTime, (decimal)_value, null, null, null, null);
                resultIndex.DataPoints.Add(resultDataPoint);
            }

            resultIndex.Name = resultIndex.Name + " | " + Name;
            Result = resultIndex;
            return resultIndex;
        }

        public PriceIndex? GetResultPriceIndex()
        {
            return Result;
        }
    }
}
