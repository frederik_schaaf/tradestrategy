﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeStrategy.Strategy
{
    public static class DefaultStrategyList
    {
        public static List<IStrategy> Strategies => new List<IStrategy>
        {
            new Strategy_Hold(),
            new Strategy_TradeOnThreshold(Strategy_TradeOnThreshold.Settings.Default),
            new Strategy_StopLoss(Strategy_StopLoss.Settings.Default),
        };
    }
}
