﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradeStrategy.DataDefinition;

namespace TradeStrategy.Strategy
{
    public class MassEvolver
    {
        public struct TypedStrategy
        {
            public IStrategy Strategy;
            public bool IsDefault;
            public string InfoText => Strategy.InfoText;
        }

        public class StrategyResult
        {
            public IStrategy Strategy;
            public double AverageAnnualROI;
            public List<Result> Results;

            public void Cummulate()
            {
                double cummulatedAnnualROI = 0;
                int n = 0;
                foreach (var result in Results)
                {
                    ++n;
                    cummulatedAnnualROI += result.ResultAnnualROI;
                }
                if (n > 0)
                {
                    AverageAnnualROI = cummulatedAnnualROI / n;
                }
            }
        }

        public struct Result
        {
            public IStrategy Strategy;
            public PriceIndex OriginalPriceIndex => Strategy.GetOriginalPriceIndex().Value;
            public PriceIndex ResultPriceIndex => Strategy.GetResultPriceIndex().Value;
            public double OriginalAnnualROI => OriginalPriceIndex.AnnualROI;
            public double ResultAnnualROI => ResultPriceIndex.AnnualROI;
        }

        public struct PriceIndexResult
        {
            public PriceIndex Original;
            public List<Result> StrategyResults;
        }

        public struct MassEvolverResult
        {
            public List<PriceIndexResult> PriceIndexResults;
            public List<StrategyResult> StrategyResults;
            public List<Result> Results;
        }

        public static event EventHandler<ProgressEventArgs> Progress;

        public class ProgressEventArgs : EventArgs
        {
            public double Progress;
        }

        private static void OnProgress(object sender, ProgressEventArgs progress)
        {
            Progress?.Invoke(sender, progress);
        }

        public static MassEvolverResult Evolve(List<PriceIndex> priceIndices, List<TypedStrategy> strategies, int nSteps)
        {
            var result = new MassEvolverResult();
            result.PriceIndexResults = new List<PriceIndexResult>();
            result.StrategyResults = new List<StrategyResult>();
            result.Results = new List<Result>();
            
            var nPriceIndices = priceIndices.Count;
            var nStrategies = strategies.Count;
            var iProgress = 0;
            var tProgress = nPriceIndices * nStrategies * nSteps;
            foreach (var originalPriceIndex in priceIndices)
            {
                var currentPriceIndexResult = new PriceIndexResult
                {
                    Original = originalPriceIndex,
                    StrategyResults = new List<Result>()
                };
                result.PriceIndexResults.Add(currentPriceIndexResult);

                var strategiesMemory = new HashSet<string>();
                foreach (var strategy in strategies)
                {
                    if (strategy.IsDefault)
                    {
                        var evolver = strategy.Strategy.CreateEvolver(nSteps);
                        
                        do
                        {
                            var eStrategy = evolver.Next();
                            if (eStrategy == null || strategiesMemory.Contains(eStrategy.Name))
                            {
                                break;
                            }
                            strategiesMemory.Add(eStrategy.Name);
                            eStrategy.Apply(originalPriceIndex);
                            var currentResult = new Result
                            {
                                Strategy = eStrategy
                            };
                            currentPriceIndexResult.StrategyResults.Add(currentResult);
                            result.Results.Add(currentResult);
                            bool hasAdded = false;
                            foreach (var strategyResult in result.StrategyResults)
                            {
                                if (strategyResult.Strategy.Equals(eStrategy))
                                {
                                    strategyResult.Results.Add(currentResult);
                                    hasAdded = true;
                                    break;
                                }
                            }
                            if (!hasAdded)
                            {
                                result.StrategyResults.Add(new StrategyResult
                                {
                                    Strategy = eStrategy,
                                    Results = new List<Result>
                                    {
                                        currentResult
                                    }
                                });
                            }
                            ++iProgress;
                            OnProgress(null, new ProgressEventArgs
                            {
                                Progress = 100.0 * iProgress / tProgress
                            });
                        } while (true);
                    }
                }
                
            }
            foreach (var strategyResult in result.StrategyResults)
            {
                strategyResult.Cummulate();
            }
            return result;
        }
    }
}
