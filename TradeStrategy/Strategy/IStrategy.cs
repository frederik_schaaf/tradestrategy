﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TradeStrategy.DataDefinition;

namespace TradeStrategy.Strategy
{
    public class Transaction
    {
        public DateTime DateTime;
        public double Price;
        public double FundsOld;
        public double FundsNew;
        public double SharesOld;
        public double SharesNew;
        public double Value;

        public double FundsReceived => -FundsSpent;
        public double FundsSpent => FundsOld - FundsNew;
        public double SharesBought => -SharesSold;
        public double SharesSold => SharesOld - SharesNew;
        public bool IsSale => FundsReceived > 0;
        public bool IsPurchase => FundsSpent > 0;

        public override string ToString()
        {
            var fundsAdd = FundsReceived >= 0 ? "+" : "";
            var sharesAdd = SharesBought >= 0 ? "+" : "";
            return $"{DateTime.Date}: Price:{Price:N4}; Value:{Value:N4}; Funds:{FundsOld:N4}({fundsAdd}{FundsReceived:N4})->{FundsNew:N4}; Shares:{SharesOld:N4}({sharesAdd}{SharesBought:N4})->{SharesNew:N4}";
        }

    }

    public interface IStrategy
    {
        PriceIndex Apply(PriceIndex original);
        List<Transaction> GetTransactions();
        string Name { get; }
        IEvolver<IStrategy> CreateEvolver(int nSteps);
        PriceIndex? GetResultPriceIndex();
        PriceIndex? GetOriginalPriceIndex();
        bool Equals(IStrategy strategy);
        IStrategy Clone();
        string InfoText { get; }
    }

    public static class Detail
    {
        public static string Serialize(IStrategy s)
        {
            return JsonConvert.SerializeObject(s, new JsonSerializerSettings
            {
               TypeNameHandling = TypeNameHandling.All
            });
        }

        public static IStrategy Deserialize(string s)
        {
            return (IStrategy)JsonConvert.DeserializeObject(s, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }

        public static void SaveToFile(IStrategy s, string path)
        {
            var str = Serialize(s);
            File.WriteAllText(path, str);
        }

        public static IStrategy LoadFromFile(string path)
        {
            var str = File.ReadAllText(path);
            return Deserialize(str);
        }
    }
}
