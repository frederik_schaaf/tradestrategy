﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using TradeStrategy.DataDefinition;

namespace TradeStrategy.Strategy
{
    public class Strategy_Hold_Evolver : IEvolver<IStrategy>
    {
        public int NSteps => 1;
        private int currentStep;

        public IStrategy Current()
        {
            return currentStep > NSteps ? null : new Strategy_Hold();
        }

        public IStrategy Next()
        {
            ++currentStep;
            return Current();
        }
    }

    public class Strategy_Hold : IStrategy
    {
        public string InfoText => "Buy in at the beginning, sell at the end. This is an exact copy of the original price index.";

        public bool Equals(IStrategy strategy)
        {
            return Name == strategy.Name;
        }

        public IEvolver<IStrategy> CreateEvolver(int nSteps)
        {
            return new Strategy_Hold_Evolver();
        }

        public string Name => "Hold";

        private PriceIndex? _result;
        private PriceIndex? _original;

        public IStrategy Clone()
        {
            return new Strategy_Hold();
        }

        public List<Transaction> GetTransactions()
        {
            return new List<Transaction>();
        }

        public PriceIndex? GetOriginalPriceIndex()
        {
            return _original;
        }

        public PriceIndex Apply(PriceIndex original)
        {
            if (_result != null)
            {
                return _result.Value;
            }
            _original = original;

            var cpy = original;
            cpy.Name = cpy.Name + " | " + Name;
            _result = cpy;
            return original;
        }

        public PriceIndex? GetResultPriceIndex()
        {
            return _result;
        }
    }
}
