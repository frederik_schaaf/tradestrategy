﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeStrategy.Strategy
{
    public interface IEvolver<out TStrategy> where TStrategy: IStrategy
    {
        TStrategy Next();
        TStrategy Current();
    }
}
