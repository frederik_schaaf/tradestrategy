﻿using TradeStrategy.Parser;
using Unity;

namespace TradeStrategy
{
    public class Unity
    {
        public static UnityContainer UnityContainer
        {
            get
            {
                if (_unityContainer == null)
                {
                    _unityContainer = new UnityContainer();
                    _unityContainer.RegisterType<IPriceIndexParser, PriceIndexParserCSV>();
                }
                return _unityContainer;
            } 
        }

        private static UnityContainer _unityContainer;
    }
}
