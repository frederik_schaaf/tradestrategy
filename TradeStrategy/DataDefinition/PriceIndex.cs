﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TradeStrategy.DataDefinition
{
    public struct PriceIndex
    {
        public struct DataPoint
        {
            public DataPoint(DateTime dateTime, decimal? price, decimal? priceOpen, decimal? priceClose,
                decimal? priceMin, decimal? priceMax)
            {
                DateTime = dateTime;
                _price = price;
                _priceOpen = priceOpen;
                _priceClose = priceClose;
                _priceMin = priceMin;
                _priceMax = priceMax;
            }

            public decimal? Scale(decimal? var, double factor)
            {
                if (!var.HasValue) return null;
                return (decimal) ((double) var * factor);
            }

            public DataPoint GetScaled(double factor)
            {
                return new DataPoint(dateTime: DateTime, price: Scale(_price, factor),
                    priceOpen: Scale(_priceOpen, factor), priceClose: Scale(_priceClose, factor),
                    priceMin: Scale(_priceMin, factor), priceMax: Scale(_priceMax, factor));
            }

            public DateTime DateTime { get; }

            public decimal? Price
            {
                get
                {
                    if (_price.HasValue)
                    {
                        return _price;
                    }
                    if (PriceClose.HasValue)
                    {
                        return PriceClose;
                    }
                    if (PriceOpen.HasValue)
                    {
                        return PriceOpen;
                    }
                    if (_priceMin.HasValue && _priceMax.HasValue)
                    {
                        return (_priceMin + _priceMax) / 2;
                    }
                    return _priceMin ?? _priceMax;
                }
            }

            public decimal? PriceMin => _priceMin ?? Min(_price, Min(_priceMax, Min(PriceOpen, PriceClose)));
            public decimal? PriceMax => _priceMax ?? Max(_price, Max(_priceMin, Max(PriceOpen, PriceClose)));
            public decimal? PriceOpen => _priceOpen ?? _price;
            public decimal? PriceClose => _priceClose ?? _price;

            private readonly decimal? _price;
            private readonly decimal? _priceMin;
            private readonly decimal? _priceMax;
            private readonly decimal? _priceOpen;
            private readonly decimal? _priceClose;

            private static decimal? Min(decimal? x, decimal? y)
            {
                if (!x.HasValue)
                {
                    return y;
                }
                if (!y.HasValue)
                {
                    return x;
                }
                return Math.Min(x.Value, y.Value);
            }

            private static decimal? Max(decimal? x, decimal? y)
            {
                if (!x.HasValue)
                {
                    return y;
                }
                if (!y.HasValue)
                {
                    return x;
                }
                return Math.Max(x.Value, y.Value);
            }

            public static DataPoint Combine(List<DataPoint> dataPoints)
            {
                var firstDateTime = dataPoints.First().DateTime;
                var lastDateTime = dataPoints.Last().DateTime;
                var dateTime = firstDateTime + new TimeSpan((lastDateTime - firstDateTime).Ticks / 2);
                var priceOpen = dataPoints.First().PriceOpen;
                var priceClose = dataPoints.Last().PriceClose;
                decimal? priceMin = null;
                decimal? priceMax = null;
                decimal sumPrice = 0;
                int nPrices = 0;
                foreach (var dataPoint in dataPoints)
                {
                    if (dataPoint.Price.HasValue)
                    {
                        sumPrice += dataPoint.Price.Value;
                        ++nPrices;
                    }
                    if (dataPoint.PriceMin.HasValue)
                    {
                        priceMin = priceMin.HasValue
                            ? Math.Min(priceMin.Value, dataPoint.PriceMin.Value)
                            : dataPoint.PriceMin.Value;
                    }
                    if (dataPoint.PriceMax.HasValue)
                    {
                        priceMax = priceMax.HasValue
                            ? Math.Max(priceMax.Value, dataPoint.PriceMax.Value)
                            : dataPoint.PriceMax.Value;
                    }
                }

                decimal? price = null;
                if (nPrices > 0)
                {
                    price = sumPrice / nPrices;
                }

                return new DataPoint(
                    dateTime,
                    price,
                    priceOpen,
                    priceClose,
                    priceMin,
                    priceMax
                    );
            }
        }
        public List<DataPoint> DataPoints;

        public double ROI
        {
            get
            {
                if (DataPoints == null || DataPoints.Count < 2)
                {
                    return 0;
                }
                var firstPrice = DataPoints.First().Price;
                var lastPrice = DataPoints.Last().Price;
                if (!firstPrice.HasValue || !lastPrice.HasValue || firstPrice == 0)
                {
                    return 0;
                }
                var result = (double)lastPrice.Value / (double)firstPrice.Value - 1;
                return result;

            }
        }

        public TimeSpan? TimeSpan
        {
            get
            {

                if (DataPoints == null || DataPoints.Count < 2)
                {
                    return null;
                }
                var firstDateTime = DataPoints.First().DateTime;
                var lastDateTime = DataPoints.Last().DateTime;
                return lastDateTime - firstDateTime;
            }
        }

        public double AnnualROI
        {
            get
            {
                if (!TimeSpan.HasValue || ROI == 0)
                {
                    return 0;
                }
                var totalYears = TimeSpan.Value.TotalDays / 365.25;
                if (totalYears == 0)
                {
                    return 0;
                }
                var totalYearsCapped = Math.Max(1, totalYears);
                var result = Math.Pow(Math.Abs(ROI + 1), 1 / totalYearsCapped) - 1;
                return result;
            }
        }

        public PriceIndex GetNormalized()
        {
            var result = this.Clone();
            if (DataPoints == null || DataPoints.Count == 0)
            {
                return result;
            }
            var factorInv = DataPoints.First().Price;
            if (!factorInv.HasValue || factorInv == 0)
            {
                return result;
            }
            var factor = 1 / (double)factorInv.Value;
            result.DataPoints = new List<DataPoint>();
            foreach (var dataPoint in DataPoints)
            {
                result.DataPoints.Add(dataPoint.GetScaled(factor));
            }
            return result;
        }

        public PriceIndex Clone()
        {
            var result = new PriceIndex();
            if (DataPoints != null)
            {
                result.DataPoints = new List<DataPoint>(DataPoints);
            }
            return result;
        }

        public PriceIndex Fold(int size)
        {
            if (DataPoints == null || DataPoints.Count <= size)
            {
                return Clone();
            }
            var result = new PriceIndex {DataPoints = new List<DataPoint>()};
            int lastUpperBound = -1;
            for (int i = 1; i <= size; ++i)
            {
                int lowerBound = lastUpperBound + 1;
                int upperBound = i * DataPoints.Count / size - 1;
                lastUpperBound = upperBound;
                var range = DataPoints.GetRange(lowerBound, upperBound - lowerBound + 1);
                var dataPoint = DataPoint.Combine(range);
                result.DataPoints.Add(dataPoint);
            }
            return result;
        }

        public String Name { get; set; }
    }

}
