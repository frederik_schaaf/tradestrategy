﻿using System.IO;
using TradeStrategy.DataDefinition;

namespace TradeStrategy.Parser
{
    public interface IPriceIndexParser
    {
        PriceIndex Parse(Stream stream);
    }
}
