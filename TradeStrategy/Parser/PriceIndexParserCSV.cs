﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using TradeStrategy.DataDefinition;

namespace TradeStrategy.Parser
{
    public class PriceIndexParserCSV : IPriceIndexParser
    {
        enum FieldType
        {
            DateTime,
            Price,
            PriceOpen,
            PriceClose,
            PriceMin,
            PriceMax
        }

        private static readonly Dictionary<FieldType, string[]> HeaderSchema = new Dictionary<FieldType, string[]>
        {
            { FieldType.DateTime, new[]{ "date", "datetime", "timedate", "datum", "snapped_at"}},
            { FieldType.Price, new[]{"price"}},
            { FieldType.PriceOpen, new[]{"erster", "open"}},
            { FieldType.PriceClose, new[]{"schlusskurs", "close"}},
            { FieldType.PriceMin, new[]{"tief", "low"}},
            { FieldType.PriceMax, new[]{"hoch", "high"}},
        };

        List<FieldType?> ParseHeader(string[] fields)
        {
            var headerSchema = new List<FieldType?>();
            var usedFields = new HashSet<FieldType>();
            foreach (var field in fields)
            {
                bool hasAdded = false;
                foreach (var headerTypeName in HeaderSchema)
                {
                    foreach (var name in headerTypeName.Value)
                    {
                        if (field.ToLower() == name)
                        {
                            headerSchema.Add(headerTypeName.Key);
                            if (!usedFields.Add(headerTypeName.Key))
                            {
                                throw new Exception("Duplicate header name: " + field);
                            };
                            hasAdded = true;
                            break;
                        }
                    }
                    if (hasAdded)
                    {
                        break;
                    }
                }
                if (!hasAdded)
                {
                    headerSchema.Add(null);
                }
            }
            if (!usedFields.Contains(FieldType.DateTime))
            {
                throw new Exception("Header Schema did not contain 'FieldType.DateTime'");
            }
            if (usedFields.Count < 2)
            {
                throw new Exception("Header Schema did not contain any price field");
            }
            return headerSchema;
        }

        DateTime ParseDateTime(string dateTimeString)
        {
            DateTime result;

            if (DateTime.TryParseExact(dateTimeString, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None,
                out result))
            {
                return result;
            }

            if (DateTime.TryParseExact(dateTimeString, "yyyy-MM-dd HH:mm:ss UTC", CultureInfo.InvariantCulture, DateTimeStyles.None,
                out result))
            {
                return result;
            }

            if (DateTime.TryParse(dateTimeString, CultureInfo.InvariantCulture, DateTimeStyles.None,
                out result))
            {
                return result;
            }

            throw new Exception("Could not parse DateTime: '" + dateTimeString + "'");
        }

        decimal? ParseDecimal(string decimalString, CultureInfo cultureInfo)
        {
            if (decimalString == string.Empty || decimalString == "null")
            {
                return null;
            }
            if (decimalString.Contains("e-"))
            {
                return 0;
            }
            return decimal.Parse(decimalString, cultureInfo);
        }

        public PriceIndex Parse(Stream stream)
        {
            var PriceIndex = new PriceIndex();
            PriceIndex.DataPoints = new List<PriceIndex.DataPoint>();
            try
            {
                var sr = new StreamReader(stream);
                var headerString = sr.ReadLine();
                var headerContainsSemicolon = headerString.Contains(";");
                stream.Position = 0;

                using (TextFieldParser parser = new TextFieldParser(stream))
                { 
                    parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                    parser.SetDelimiters(headerContainsSemicolon ? ";" : ",");
                    var cultureInfo = headerContainsSemicolon ? new CultureInfo("DE-de") : CultureInfo.InvariantCulture;
                    List<FieldType?> headerSchema = null;
                    while (!parser.EndOfData)
                    {
                        var fields = parser.ReadFields();
                        if (headerSchema == null)
                        {
                            headerSchema = ParseHeader(fields);
                        }
                        else
                        {
                            DateTime? dateTime = null;
                            decimal? price = null;
                            decimal? priceOpen = null;
                            decimal? priceClose = null;
                            decimal? priceMin = null;
                            decimal? priceMax = null;
                            var headerSchemaEnumerator = headerSchema.GetEnumerator();
                            foreach (var field in fields)
                            {
                                headerSchemaEnumerator.MoveNext();
                                if (!headerSchemaEnumerator.Current.HasValue)
                                {
                                    continue;
                                }
                                switch (headerSchemaEnumerator.Current)
                                {
                                    case FieldType.DateTime:
                                        dateTime = ParseDateTime(field);
                                        break;
                                    case FieldType.Price:
                                        price = ParseDecimal(field, cultureInfo);
                                        break;
                                    case FieldType.PriceOpen:
                                        priceOpen = ParseDecimal(field, cultureInfo);
                                        break;
                                    case FieldType.PriceClose:
                                        priceClose = ParseDecimal(field, cultureInfo);
                                        break;
                                    case FieldType.PriceMin:
                                        priceMin = ParseDecimal(field, cultureInfo);
                                        break;
                                    case FieldType.PriceMax:
                                        priceMax = ParseDecimal(field, cultureInfo);
                                        break;
                                    default:
                                        throw new Exception("Unhandled FieldType: " + headerSchemaEnumerator.Current);
                                }
                            }
                            headerSchemaEnumerator.Dispose();
                            PriceIndex.DataPoints.Add(new PriceIndex.DataPoint(
                                    dateTime.Value,
                                    price,
                                    priceOpen,
                                    priceClose,
                                    priceMin,
                                    priceMax
                                )
                            );
                        }
                    }
                    PriceIndex.DataPoints = PriceIndex.DataPoints.OrderBy(o => o.DateTime).ToList();
                }
                return PriceIndex;
            }
            catch (Exception e)
            {
                throw new ParseException(e.Message, e);
            }
        }
    }
}
