﻿using System;
using System.IO;
using System.Windows;
using Visualizer.Evolution;

namespace TradeStrategyGUI
{
    public class App : Application
    {
        [STAThread]
        public static void Main()
        {
            Application app = new Application();
            var ui = new UI();
            ui.PriceIndex_LoadDirectory(Path.Combine(Directory.GetCurrentDirectory(),"Resources","_current"));
            app.Run(ui);
        }
    }
}
